import { Component, OnInit } from "@angular/core";
import { Course } from "../course";
import { CourseService } from "../course.service";

@Component({
  selector: "app-course",
  templateUrl: "./course.component.html",
  styleUrls: ["./course.component.css"],
})
export class CourseComponent implements OnInit {
  courses: Course[] = [];
  course: Course = new Course();

  constructor(private courseService: CourseService) {}

  ngOnInit(): void {
    this.getCourses();
  }

  getCourses(): void {
    this.courseService
      .getCourses()
      .subscribe((courses) => (this.courses = courses));
  }

  delete(course: Course): void {
    const courseMatch = this.courseService.courses.find(
      (x) => x.id === course.id
    );
    if (courseMatch) {
      courseMatch.isInProgress = false;
    }
    /*if (course) {
      this.courses = this.courses.filter((c) => c !== course);
      this.courseService.deleteCourse(course.id).subscribe();
    }*/
  }

  add(course: Course | undefined): void {
    if (course && course.name) {
      course.name = course.name.trim();
      const ids = this.courseService.courses.map((course) => {
        return course.id;
      });
      const max = Math.max(...(ids as number[]));
      course.id = max + 1;
      course.isInProgress = true;
      if (
        !this.courseService.courses.find(
          (x) =>
            x.id === course.id ||
            x.name?.toLowerCase() === course.name?.toLocaleLowerCase()
        )
      ) {
        this.courseService.courses.push(course);
        this.course = new Course();
      }
      /*this.courseService.addCourse(course).subscribe((course) => {
        this.courses.push(course);
      });*/
    }
    return;
  }
}
