export class Course {
  id?: number;
  name?: string;
  isInProgress?: boolean;

  constructor(id?: number, name?: string, isInProgress?: boolean) {
    this.id = id;
    this.name = name;
    this.isInProgress = isInProgress;
  }
}
